/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

import java.util.Date;

/**
 *
 * @author jacobwhitlock
 */
public class MinerRobotFactory implements RobotFactoryInterface{
    @Override
    public Robot makeRobot(String name) {
	MinerRobot robot = new MinerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.MINER);
	return robot;
    }

}
