package singleton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jacobwhitlock
 */
public class GardenerRobot extends Robot{
    @Override
    public void showSkill() {
	System.out.println("Manicuring your lawn to 99.999% perfection!");
    }

    @Override
    public void doWork() {
	System.out.println("Mowing the lawn and trimming the hedges.");
    }
}
