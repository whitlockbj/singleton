/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

import java.util.Date;

/**
 *
 * @author jacobwhitlock
 */
public class ButlerAndroid {
    private String androidName;
    private Date dateBuilt;

    public String getAndroidName() {
	return androidName;
    }

    public void setAndroidName(String androidName) {
	this.androidName = androidName;
    }

    public Date getDateBuilt() {
	return dateBuilt;
    }

    public void setDateBuilt(Date dateBuilt) {
	this.dateBuilt = dateBuilt;
    }

    public void answerDoor() {
	System.out.println("Answering the door.");
    }

    public void giveDemonstration() {
	System.out.println("Answering door and greeting guests!");
    }

    public void greetGuest() {
	System.out
		.println("Welcome to my owner's humble abode. Please make yourselves at home.");
    }

    public void sayWhatYouDo() {
	System.out.println("Greetings, my name is " + androidName
		+ " and I am a supremely intelligent butler unit.");
    }
}
