/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

import java.util.Date;

/**
 *
 * @author jacobwhitlock
 */
public class ButlerRobot extends Robot {
    private ButlerAndroid butlerAndroid = new ButlerAndroid();

    @Override
    public void setName(String name) {
	super.setName(name);
	butlerAndroid.setAndroidName(name);
    }

    @Override
    public String getName() {
	return butlerAndroid.getAndroidName();
    }

    @Override
    public void setDateCreated(Date dateCreated) {
	super.setDateCreated(dateCreated);
	butlerAndroid.setDateBuilt(dateCreated);
    }

    @Override
    public Date getDateCreated() {
	return butlerAndroid.getDateBuilt();
    }

    @Override
    public void showSkill() {
	butlerAndroid.giveDemonstration();
    }

    @Override
    public void doWork() {
	butlerAndroid.answerDoor();
	butlerAndroid.greetGuest();
    }
}
