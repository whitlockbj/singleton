package singleton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jacobwhitlock
 */
public class SteakMealBuilder extends MealBuilder{
    @Override
    public void buildMainCourse() {
	meal.setMainCourse("Porterhouse, Medium Well");
    }

    @Override
    public void buildDrink() {
	meal.setDrink("Beer");
    }

    @Override
    public void buildDessert() {
	meal.setDessert("Texas Cheesecake");
    }

    @Override
    public void buildSideDish() {
	meal.setSideDish("Ceasers Salad");
    }

    @Override
    public Meal getMeal() {
	return meal;
    }
}
