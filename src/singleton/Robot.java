/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

import java.util.Date;

/**
 *
 * @author jacobwhitlock
 */

    public abstract class Robot implements Cloneable {
    protected String name;
    protected Role role;
    protected Date dateCreated;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Role getRole() {
	return role;
    }

    public void setRole(Role role) {
	this.role = role;
    }

    public Date getDateCreated() {
	return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
	this.dateCreated = dateCreated;
    }

    public void introduce() {
	System.out.println("Greetings, my name is " + name + " and I am a "
		+ role.name().toLowerCase() + ". I was born on " + dateCreated);
    }

    @Override
    public Robot clone() {
	Robot clonedRobot = null;
	try {
	    clonedRobot = (Robot) super.clone();
	} catch (CloneNotSupportedException e) {
	    e.printStackTrace();
	}
	return clonedRobot;
    }

    public abstract void showSkill();

    public abstract void doWork();
}


