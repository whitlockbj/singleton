/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

/**
 *
 * @author jacobwhitlock
 */
public class ItalianMealBuilder extends MealBuilder{
    @Override
    public void buildMainCourse() {
	meal.setMainCourse("Pasta al Forno");
    }

    @Override
    public void buildDrink() {
	meal.setDrink("Adams Reserve Pinot Noir");
    }

    @Override
    public void buildDessert() {
	meal.setDessert("Tiramisu");
    }

    @Override
    public void buildSideDish() {
	meal.setSideDish("Italian Wedding Soup");
    }

    @Override
    public Meal getMeal() {
	return meal;
    }

}
