package singleton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jacobwhitlock
 */
public class MealDirector {
     public Meal createMeal(MealBuilder builder) {
	builder.buildMainCourse();
	builder.buildSideDish();
	builder.buildDessert();
	builder.buildDrink();
	return builder.getMeal();
    }
}
