/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package singleton;

import java.util.Date;

/**
 *
 * @author jacobwhitlock
 */
public class ChefRobotFactory implements RobotFactoryInterface {
    @Override
    public Robot makeRobot(String name) {
	ChefRobot robot = new ChefRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.CHEF);

	return robot;
    }

}
